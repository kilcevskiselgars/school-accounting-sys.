<?php
require 'config.php';

spl_autoload_register(function ($name) {
    require_once(str_replace('\\',DS,APPLICATION_PATH.DS.$name.'.php'));
});

$uri = $_SERVER['REQUEST_URI'];

try {
    require 'routes.php';
} catch (Exception $exception) {
    print('<pre>'.$exception->getMessage().$exception->getFile().'<pre>');
    print('<pre>'.'In line: '.$exception->getLine().'<pre>');
    print('<pre>'.$exception->getTraceAsString().'<pre>');
    print('<pre>'.$exception->getPrevious().'<pre>');
}
