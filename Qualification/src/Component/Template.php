<?php
namespace Component;

use Exception;

class Template
{
    private $data;

    public function assign(array $data)
    {
        $this->data = $data;
    }

    public function render(string $file) : string
    {
        if(is_array($this->data)) extract($this->data);

        $filePath = str_replace('\\',DS,TEMPLATE_PATH.DS.$file);

        try{
            ob_start();
            if (file_exists($filePath)) {
                require $filePath;
                return ob_end_flush();
            }
            else {
                throw new Exception('File does not exists!');
            }

        } catch (Exception $exception) {
            throw new Exception('Template is not valid! ',0,$exception);
        }
    }
}
