<?php

use Component\Template;

$uri = $_SERVER['REQUEST_URI'];

switch ($uri) {
    case '/';
        echo 'hello';
        break;

    case '/login';
        (new Template)->render('hello-world.html');
        break;

    default;
        (new Template)->render('404.html');
}
