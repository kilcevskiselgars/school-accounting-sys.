CREATE TABLE classes
(
    id        int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)
);

CREATE TABLE students
(
    id       int PRIMARY KEY AUTO_INCREMENT,
    firstname    varchar(255),
    lastname  varchar(255),
    classes_id int
);

CREATE TABLE teachers
(
    id      int PRIMARY KEY AUTO_INCREMENT,
    firstname   varchar(255),
    lastname varchar(255),
    email  varchar(255),
    password  varchar(255),
    admin   boolean
);

CREATE TABLE subjects
(
    id        int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)
);

CREATE TABLE teacherSubjects
(
    id            int PRIMARY KEY AUTO_INCREMENT,
    teacher_id  int,
    subjects_id int
);

CREATE TABLE main
(
    id                     int PRIMARY KEY AUTO_INCREMENT,
    student_id           int,
    classes_id               int,
    teacherSubject_id int
);

ALTER TABLE students
    ADD FOREIGN KEY (classes_id) REFERENCES classes (id);
ALTER TABLE teacherSubjects
    ADD FOREIGN KEY (subjects_id) REFERENCES subjects (id);
ALTER TABLE teacherSubjects
    ADD FOREIGN KEY (teacher_id) REFERENCES teachers (id);
ALTER TABLE main
    ADD FOREIGN KEY (classes_id) REFERENCES classes (id);
ALTER TABLE main
    ADD FOREIGN KEY (teacherSubject_id) REFERENCES teacherSubjects (id);
ALTER TABLE main
    ADD FOREIGN KEY (student_id) REFERENCES students (id);